package ru.itshool.phonebook.aggregates;

public enum ContactType {
    EMAIL,
    SKYPE,
    PHONE_NUMBER,
    ADDRESS
}
