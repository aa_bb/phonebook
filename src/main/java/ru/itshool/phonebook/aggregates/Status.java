package ru.itshool.phonebook.aggregates;

public enum Status {
    ACTIVATED,
    ARCHIVED
}
