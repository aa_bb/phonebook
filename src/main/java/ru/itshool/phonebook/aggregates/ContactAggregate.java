package ru.itshool.phonebook.aggregates;

import org.axonframework.commandhandling.CommandHandler;
import org.axonframework.eventsourcing.EventSourcingHandler;
import org.axonframework.modelling.command.AggregateIdentifier;
import org.axonframework.modelling.command.AggregateLifecycle;
import org.axonframework.spring.stereotype.Aggregate;
import ru.itshool.phonebook.commands.CreateContactCommand;
import ru.itshool.phonebook.commands.MergePersonsCommand;
import ru.itshool.phonebook.events.ContactCreatedEvent;
import ru.itshool.phonebook.events.PersonsMergedEvent;

@Aggregate
public class ContactAggregate {
    @AggregateIdentifier
    private String id;
    private String contactType;
    private String value;
    private String personId;

    public String getId() {
        return id;
    }

    public String getContactType() {
        return contactType;
    }

    public String getValue() {
        return value;
    }

    public String getPersonId() {  return personId; }

    public ContactAggregate() {}

    @CommandHandler
    public ContactAggregate(CreateContactCommand createContactCommand){
        AggregateLifecycle.apply(new ContactCreatedEvent(
                createContactCommand.getId(), createContactCommand.getContactType(),
                createContactCommand.getValue(), createContactCommand.getPersonId()));
    }

    @EventSourcingHandler
    protected void on(ContactCreatedEvent contactCreatedEvent){
        this.id = contactCreatedEvent.getId();
        this.contactType = String.valueOf(contactCreatedEvent.getContactType());
        this.value = contactCreatedEvent.getValue();
        this.personId = contactCreatedEvent.getPersonId();
    }

    @CommandHandler
    protected void on(MergePersonsCommand mergePersonsCommand){
        AggregateLifecycle.apply(new PersonsMergedEvent(mergePersonsCommand.getId(),
                mergePersonsCommand.getPersonId(), mergePersonsCommand.getMergingPersonId()));
    }

    @EventSourcingHandler
    protected void on(PersonsMergedEvent personsMergedEvent) {
        if (this.personId.equals(personsMergedEvent.getId()))
            this.personId = personsMergedEvent.getMergingPersonId();
    }
}
