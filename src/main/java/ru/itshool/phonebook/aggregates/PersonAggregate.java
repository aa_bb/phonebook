package ru.itshool.phonebook.aggregates;

import org.axonframework.commandhandling.CommandHandler;
import org.axonframework.eventsourcing.EventSourcingHandler;
import org.axonframework.modelling.command.AggregateIdentifier;
import org.axonframework.modelling.command.AggregateLifecycle;
import org.axonframework.spring.stereotype.Aggregate;
import ru.itshool.phonebook.commands.CreatePersonCommand;
import ru.itshool.phonebook.commands.DeletePersonCommand;
import ru.itshool.phonebook.events.PersonCreatedEvent;
import ru.itshool.phonebook.events.PersonDeletedEvent;

@Aggregate
public class PersonAggregate {
    @AggregateIdentifier
    private String id;
    private String username;
    private String birthdate;
    private String status;

    public String getId() {
        return id;
    }

    public String getUsername() {
        return username;
    }

    public String getBirthdate() {
        return birthdate;
    }

    public String getStatus() {
        return status;
    }

    public PersonAggregate() {}

    @CommandHandler
    public PersonAggregate(CreatePersonCommand createPersonCommand){
        AggregateLifecycle.apply(new PersonCreatedEvent(
                createPersonCommand.getId(),
                createPersonCommand.getUsername(),
                createPersonCommand.getBirthdate()
        ));
    }

    @EventSourcingHandler
    protected void on(PersonCreatedEvent personCreatedEvent){
        this.id = personCreatedEvent.getId();
        this.username = personCreatedEvent.getUsername();
        this.birthdate = personCreatedEvent.getBirthdate();
        this.status = String.valueOf(Status.ACTIVATED);
    }

    @CommandHandler
    protected void on(DeletePersonCommand deletePersonCommand){
        AggregateLifecycle.apply(new PersonDeletedEvent(deletePersonCommand.getId()));
    }

    @EventSourcingHandler
    protected void on(PersonDeletedEvent personDeletedEvent){
        this.status = String.valueOf(Status.ARCHIVED);
    }
}
