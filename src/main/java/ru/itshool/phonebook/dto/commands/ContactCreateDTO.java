package ru.itshool.phonebook.dto.commands;

public class ContactCreateDTO {
    private String value;

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}

