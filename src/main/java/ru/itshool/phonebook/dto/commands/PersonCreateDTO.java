package ru.itshool.phonebook.dto.commands;

public class PersonCreateDTO {
    private String username;
    private String birthdate;

    public String getUsername() {
        return username;
    }

    public String getBirthdate() {
        return birthdate;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setBirthdate(String birthdate) {
        this.birthdate = birthdate;
    }
}
