package ru.itshool.phonebook.dto.commands;

public class PersonsMergeDTO {
    private String mergingPersonId;

    public String getMergingPersonId() {
        return mergingPersonId;
    }

    public void setMergingPersonId(String mergingPersonId) {
        this.mergingPersonId = mergingPersonId;
    }
}
