package ru.itshool.phonebook.commands;

public class DeletePersonCommand extends BaseCommand<String> {
    public DeletePersonCommand(String id) {
        super(id);
    }
}
