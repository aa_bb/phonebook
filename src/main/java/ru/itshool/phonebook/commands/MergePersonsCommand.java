package ru.itshool.phonebook.commands;

public class MergePersonsCommand extends BaseCommand<String> {
    private final String personId;
    private final String mergingPersonId;

    public String getMergingPersonId() {
        return mergingPersonId;
    }

    public String getPersonId() {
        return personId;
    }

    public MergePersonsCommand(String id, String personId, String mergingPersonId) {
        super(id);
        this.mergingPersonId = mergingPersonId;
        this.personId = personId;
    }
}
