package ru.itshool.phonebook.commands;

import org.axonframework.modelling.command.TargetAggregateIdentifier;

public class BaseCommand<T> {
    @TargetAggregateIdentifier
    private final T id;

    public T getId() {
        return id;
    }

    public BaseCommand(T id) {
        this.id = id;
    }
}
