package ru.itshool.phonebook.events;

public class BaseEvent<T> {
    private final T id;

    public T getId() {
        return id;
    }

    public BaseEvent(T id) {
        this.id = id;
    }
}