package ru.itshool.phonebook.events;

public class PersonDeletedEvent extends BaseEvent<String> {
    public PersonDeletedEvent(String id) { super(id); }
}
