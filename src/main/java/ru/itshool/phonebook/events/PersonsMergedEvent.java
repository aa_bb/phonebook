package ru.itshool.phonebook.events;

public class PersonsMergedEvent extends BaseEvent<String> {
    private final String mergingPersonId;
    private final String personId;

    public String getMergingPersonId() {
        return mergingPersonId;
    }

    public String getPersonId() {
        return personId;
    }

    public PersonsMergedEvent(String id, String personId, String mergingPersonId) {
        super(id);
        this.mergingPersonId = mergingPersonId;
        this.personId = personId;
    }
}
