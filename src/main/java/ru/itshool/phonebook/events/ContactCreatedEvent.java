package ru.itshool.phonebook.events;

import ru.itshool.phonebook.aggregates.ContactType;

public class ContactCreatedEvent extends BaseEvent<String> {
    private final ContactType contactType;
    private final String value;
    private final String personId;

    public ContactType getContactType() {
        return contactType;
    }

    public String getValue() {
        return value;
    }

    public String getPersonId() {
        return personId;
    }

    public ContactCreatedEvent(String id, ContactType contactType, String value, String personId) {
        super(id);
        this.contactType = contactType;
        this.value = value;
        this.personId = personId;
    }
}
