package ru.itshool.phonebook.events;

public class PersonCreatedEvent extends BaseEvent<String> {
    private final String username;
    private final String birthdate;

    public String getUsername() {
        return username;
    }

    public String getBirthdate() {
        return birthdate;
    }

    public PersonCreatedEvent(String id, String username, String birthdate) {
        super(id);
        this.username = username;
        this.birthdate = birthdate;
    }
}
