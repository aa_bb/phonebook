package ru.itshool.phonebook.services.commands;

import ru.itshool.phonebook.aggregates.ContactType;
import ru.itshool.phonebook.dto.commands.ContactCreateDTO;
import ru.itshool.phonebook.dto.commands.PersonCreateDTO;
import ru.itshool.phonebook.dto.commands.PersonsMergeDTO;

import java.util.concurrent.CompletableFuture;

public interface PersonCommandService {
    public CompletableFuture<String> createContact(String personId, ContactCreateDTO contactCreateDTO, ContactType contactType);
    public CompletableFuture<String> createPerson(PersonCreateDTO personCreateDTO);
    public CompletableFuture<String> mergePersons(String contactId, String personId, PersonsMergeDTO personsMergeDTO);
    public CompletableFuture<String> deletePerson(String personId);

}