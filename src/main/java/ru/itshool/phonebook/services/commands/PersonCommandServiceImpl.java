package ru.itshool.phonebook.services.commands;

import org.axonframework.commandhandling.gateway.CommandGateway;
import org.springframework.stereotype.Service;
import ru.itshool.phonebook.aggregates.ContactType;
import ru.itshool.phonebook.commands.CreateContactCommand;
import ru.itshool.phonebook.commands.CreatePersonCommand;
import ru.itshool.phonebook.commands.DeletePersonCommand;
import ru.itshool.phonebook.commands.MergePersonsCommand;
import ru.itshool.phonebook.dto.commands.ContactCreateDTO;
import ru.itshool.phonebook.dto.commands.PersonCreateDTO;
import ru.itshool.phonebook.dto.commands.PersonsMergeDTO;

import java.util.UUID;
import java.util.concurrent.CompletableFuture;

@Service
public class PersonCommandServiceImpl implements PersonCommandService {
    private final CommandGateway commandGateway;
    public PersonCommandServiceImpl(CommandGateway commandGateway) {
        this.commandGateway = commandGateway;
    }

    @Override
    public CompletableFuture<String> createContact(String personId, ContactCreateDTO contactCreateDTO, ContactType contactType) {
        return commandGateway.send(new CreateContactCommand(UUID.randomUUID().toString(),
                contactType, contactCreateDTO.getValue(), personId));
    }

    @Override
    public CompletableFuture<String> createPerson(PersonCreateDTO personCreateDTO) {
        return commandGateway.send(new CreatePersonCommand(UUID.randomUUID().toString(),
                personCreateDTO.getUsername(), personCreateDTO.getBirthdate()));
    }

    @Override
    public CompletableFuture<String> mergePersons(String contactId, String personId, PersonsMergeDTO personsMergeDTO) {
        return commandGateway.send(new MergePersonsCommand(contactId, personId, personsMergeDTO.getMergingPersonId()));
    }

    @Override
    public CompletableFuture<String> deletePerson(String personId) {
        return commandGateway.send(new DeletePersonCommand(personId));
    }
}
