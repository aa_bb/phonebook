package ru.itshool.phonebook.services.queries;

import ru.itshool.phonebook.entities.ContactQueryEntity;
import ru.itshool.phonebook.entities.PersonQueryEntity;

import java.util.List;
import java.util.concurrent.CompletableFuture;

public interface PersonQueryService {
    public List<Object> listEventsForPerson(String personId);
    public PersonQueryEntity getPerson(String personId);
    public ContactQueryEntity getContact(String contactId);
    public List<Object> getAllPersons();
    public List<Object> findDuplicates();
    public List<Object> getPersonContacts(String personId);
    public List<Object> findPersonByData(String value);
    public List<Object> findPersonByContact(String value);
}