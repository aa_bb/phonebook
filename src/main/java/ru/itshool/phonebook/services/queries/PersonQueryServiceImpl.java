package ru.itshool.phonebook.services.queries;

import org.axonframework.eventsourcing.eventstore.EventStore;
import org.springframework.stereotype.Service;
import ru.itshool.phonebook.entities.ContactQueryEntity;
import ru.itshool.phonebook.entities.PersonQueryEntity;
import ru.itshool.phonebook.entities.repositories.ContactRepository;
import ru.itshool.phonebook.entities.repositories.PersonRepository;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Service
public class PersonQueryServiceImpl implements PersonQueryService {
    private final EventStore eventStore;
    private final PersonRepository personRepository;
    private final ContactRepository contactRepository;

    public PersonQueryServiceImpl(EventStore eventStore, PersonRepository personRepository, ContactRepository contactRepository) {
        this.eventStore = eventStore;
        this.personRepository = personRepository;
        this.contactRepository = contactRepository;
    }

    @Override
    public List<Object> listEventsForPerson(String personId) {
        return eventStore.readEvents(personId).asStream().map(s -> s.getPayload()).collect(Collectors.toList());
    }

    @Override
    public PersonQueryEntity getPerson(String personId) {
        return personRepository.findById(personId).get();
    }

    @Override
    public List<Object> getAllPersons() {
        return StreamSupport.stream(personRepository.findAll().spliterator(), false).collect(Collectors.toList());
    }

    @Override
    public List<Object> findDuplicates() {
        List<PersonQueryEntity> persons = StreamSupport.stream(personRepository.findAll().spliterator(), false).collect(Collectors.toList());;
        return  persons.stream()
                .filter(i -> Collections.frequency(getAllPersons(), i) > 1)
                .collect(Collectors.toList());
    }

    private List<ContactQueryEntity> getAllContacts() {
        return StreamSupport.stream(contactRepository.findAll().spliterator(), false).collect(Collectors.toList());
    }

    @Override
    public List<Object> getPersonContacts(String personId) {
        return getAllContacts().stream().filter(c -> c.getPersonId().equals(personId)).collect(Collectors.toList());
    }

    @Override
    public ContactQueryEntity getContact(String contactId) {
        return contactRepository.findById(contactId).get();
    }

    @Override
    public List<Object> findPersonByData(String value) {
        return getAllPersons().stream().filter(p -> p.toString().contains(value)).collect(Collectors.toList());
    }

    @Override
    public List<Object> findPersonByContact(String value) {
        return getAllContacts().stream().filter(c -> c.toString().contains(value))
                .map(ContactQueryEntity::getId).map(this::getPerson).collect(Collectors.toList());
    }
}