package ru.itshool.phonebook.controllers;

import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.*;
import ru.itshool.phonebook.aggregates.ContactType;
import ru.itshool.phonebook.dto.commands.ContactCreateDTO;
import ru.itshool.phonebook.dto.commands.PersonCreateDTO;
import ru.itshool.phonebook.dto.commands.PersonsMergeDTO;
import ru.itshool.phonebook.services.commands.PersonCommandService;

import java.util.concurrent.CompletableFuture;

@RestController
@RequestMapping(value = "/phonebook")
@Api(value = "Person Commands", description = "Person Commands Related Endpoints", tags = "Person Commands")
public class PersonCommandController {
    private final PersonCommandService personCommandService;

    public PersonCommandController(PersonCommandService personCommandService) {
        this.personCommandService = personCommandService;
    }

    @PostMapping(value = "/person/")
    public CompletableFuture<String> createPerson(@RequestBody PersonCreateDTO personCreateDTO){
        return personCommandService.createPerson(personCreateDTO);
    }

    @PostMapping(value = "/person/{personId}/email")
    public CompletableFuture<String> createEmailContact(@PathVariable(value = "personId") String personId,
                                                   @RequestBody ContactCreateDTO contactCreateDTO){
        return personCommandService.createContact(personId, contactCreateDTO, ContactType.EMAIL);
    }

    @PostMapping(value = "/person/{personId}/phone")
    public CompletableFuture<String> createPhoneContact(@PathVariable(value = "personId") String personId,
                                                   @RequestBody ContactCreateDTO contactCreateDTO){
        return personCommandService.createContact(personId, contactCreateDTO, ContactType.PHONE_NUMBER);
    }

    @PostMapping(value = "/person/{personId}/skype")
    public CompletableFuture<String> createSkypeContact(@PathVariable(value = "personId") String personId,
                                                   @RequestBody ContactCreateDTO contactCreateDTO){
        return personCommandService.createContact(personId, contactCreateDTO, ContactType.SKYPE);
    }

    @PostMapping(value = "/person/{personId}/address")
    public CompletableFuture<String> createAddressContact(@PathVariable(value = "personId") String personId,
                                                   @RequestBody ContactCreateDTO contactCreateDTO){
        return personCommandService.createContact(personId, contactCreateDTO, ContactType.ADDRESS);
    }

    @PutMapping(value = "/person/{personId}/{contactId}")
    public CompletableFuture<String> mergePersons(@PathVariable(value = "personId") String personId,
                                                  @PathVariable(value = "contactId") String contactId,
                                                           @RequestBody PersonsMergeDTO personsMergeDTO){
        return personCommandService.mergePersons(contactId, personId, personsMergeDTO);
    }

    @DeleteMapping(value = "/person/{personId}")
    public CompletableFuture<String> deletePerson(@PathVariable(value = "personId") String personId) {
        return personCommandService.deletePerson(personId);
    }
}
