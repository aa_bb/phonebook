package ru.itshool.phonebook.controllers;

import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.*;
import ru.itshool.phonebook.aggregates.ContactType;
import ru.itshool.phonebook.entities.PersonQueryEntity;
import ru.itshool.phonebook.services.queries.PersonQueryService;

import java.util.List;

@RestController
@RequestMapping(value = "/phonebook")
@Api(value = "Person Queries", description = "Person Query Events Endpoint", tags = "Person Queries")
public class PersonQueryController {
    private final PersonQueryService personQueryService;

    public PersonQueryController(PersonQueryService personQueryService) {
        this.personQueryService = personQueryService;
    }

    @GetMapping("/{personId}")
    public PersonQueryEntity getPerson(@PathVariable(value = "personId") String personId){
        return personQueryService.getPerson(personId);
    }

    @GetMapping("/{personId}/events")
    public List<Object> listEventsForPerson(@PathVariable(value = "personId") String personId) {
        return personQueryService.listEventsForPerson(personId);
    }

    @GetMapping("/person/all")
    public List<Object> getAllPersons() {
        return personQueryService.getAllPersons();
    }

    @GetMapping("/person/findDuplicates")
    public List<Object> findDuplicates() {
        return personQueryService.findDuplicates();
    }

    @GetMapping("/person/{personId}/contacts")
    public List<Object> getPersonContacts(@PathVariable(value = "personId") String personId) {
        return personQueryService.getPersonContacts(personId);
    }

    @PostMapping(value = "/person/{contactId}")
    public String sendMessageByContact(@PathVariable(value = "contactId") String contactId) {
        ContactType type = personQueryService.getContact(contactId).getContactType();
        switch (type) {
            case EMAIL:
                return "email sended";
            case PHONE_NUMBER:
                return "phone call";
            case SKYPE:
                return "skype call";
            case ADDRESS:
                return "send post package";
        }
        return "Something went wrong...";
    }

    @GetMapping("/person/findByData/{value}")
    public List<Object> findPersonByData(@PathVariable(value = "value") String value) {
        return personQueryService.findPersonByData(value);
    }

    @GetMapping("/person/findByContact/{value}")
    public List<Object> findPersonByContact(@PathVariable(value = "value") String value) {
        return personQueryService.findPersonByContact(value);
    }
}
