package ru.itshool.phonebook.config;

import org.axonframework.eventsourcing.EventSourcingRepository;
import org.axonframework.eventsourcing.eventstore.EventStore;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import ru.itshool.phonebook.aggregates.ContactAggregate;
import ru.itshool.phonebook.aggregates.PersonAggregate;

@Configuration
public class AxonConfig {
    @Bean
    EventSourcingRepository<PersonAggregate> personAggregateEventSourcingRepository(EventStore eventStore){
        EventSourcingRepository<PersonAggregate> repository = EventSourcingRepository.builder(PersonAggregate.class)
                .eventStore(eventStore).build();
        return repository;
    }

    @Bean
    EventSourcingRepository<ContactAggregate> contactAggregateEventSourcingRepository(EventStore eventStore){
        EventSourcingRepository<ContactAggregate> repository = EventSourcingRepository.builder(ContactAggregate.class)
                .eventStore(eventStore).build();
        return repository;
    }
}
