package ru.itshool.phonebook.entities.repositories;

import org.springframework.data.repository.CrudRepository;
import ru.itshool.phonebook.entities.ContactQueryEntity;

public interface ContactRepository extends CrudRepository<ContactQueryEntity, String> {
}
