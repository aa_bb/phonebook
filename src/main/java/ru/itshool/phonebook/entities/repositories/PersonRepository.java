package ru.itshool.phonebook.entities.repositories;

import org.springframework.data.repository.CrudRepository;
import ru.itshool.phonebook.entities.PersonQueryEntity;

public interface PersonRepository extends CrudRepository<PersonQueryEntity, String> {
}
