package ru.itshool.phonebook.entities;

import ru.itshool.phonebook.aggregates.ContactType;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class ContactQueryEntity {
    @Id
    private String id;
    private ContactType contactType;
    private String value;
    private String personId;

    public ContactQueryEntity() {}

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public ContactType getContactType() {
        return contactType;
    }

    public void setContactType(ContactType contactType) {
        this.contactType = contactType;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getPersonId() {
        return personId;
    }

    public void setPersonId(String personId) {
        this.personId = personId;
    }

    @Override
    public String toString() {
        return "ContactQueryEntity{" +
                "id='" + id + '\'' +
                ", contactType='" + contactType + '\'' +
                ", value='" + value + '\'' +
                ", personId='" + personId + '\'' +
                '}';
    }
}
