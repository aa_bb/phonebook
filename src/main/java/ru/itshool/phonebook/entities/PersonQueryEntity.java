package ru.itshool.phonebook.entities;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class PersonQueryEntity {
    @Id
    private String id;
    private String username;
    private String birthdate;
    private String status;

    public PersonQueryEntity() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getBirthdate() {
        return birthdate;
    }

    public void setBirthdate(String birthdate) {
        this.birthdate = birthdate;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "PersonQueryEntity{" +
                "id='" + id + '\'' +
                ", username='" + username + '\'' +
                ", birthdate='" + birthdate + '\'' +
                ", status='" + status + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (!(o instanceof PersonQueryEntity))
            return false;
        PersonQueryEntity p = (PersonQueryEntity) o;
        return username.equals(p.getUsername()) && birthdate.equals(p.getBirthdate());
    }

    @Override
    public int hashCode() {
        return username.hashCode() ^ birthdate.hashCode();
    }
}
