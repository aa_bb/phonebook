package ru.itshool.phonebook.entities.handlers;

import org.axonframework.eventsourcing.EventSourcingHandler;
import org.axonframework.eventsourcing.EventSourcingRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import ru.itshool.phonebook.aggregates.PersonAggregate;
import ru.itshool.phonebook.entities.PersonQueryEntity;
import ru.itshool.phonebook.entities.repositories.PersonRepository;
import ru.itshool.phonebook.events.BaseEvent;

@Component
public class PersonQueryEntityManager {
    @Autowired
    private PersonRepository personRepository;

    @Autowired
    @Qualifier("personAggregateEventSourcingRepository")
    private EventSourcingRepository<PersonAggregate> personAggregateEventSourcingRepository;

    @EventSourcingHandler
    void on(BaseEvent event){
        persistPerson(buildQueryPerson(getPersonFromEvent(event)));
    }


    private PersonAggregate getPersonFromEvent(BaseEvent event){
        return personAggregateEventSourcingRepository.load(event.getId().toString()).getWrappedAggregate().getAggregateRoot();
    }

    private PersonQueryEntity findExistingOrCreateQueryPerson(String id){
        return personRepository.findById(id).isPresent() ? personRepository.findById(id).get() : new PersonQueryEntity();
    }

    private PersonQueryEntity buildQueryPerson(PersonAggregate personAggregate){
        PersonQueryEntity personQueryEntity = findExistingOrCreateQueryPerson(personAggregate.getId());

        personQueryEntity.setId(personAggregate.getId());
        personQueryEntity.setUsername(personAggregate.getUsername());
        personQueryEntity.setBirthdate(personAggregate.getBirthdate());
        personQueryEntity.setStatus(personAggregate.getStatus());

        return personQueryEntity;
    }

    private void persistPerson(PersonQueryEntity personQueryEntity){
        personRepository.save(personQueryEntity);
    }
}
