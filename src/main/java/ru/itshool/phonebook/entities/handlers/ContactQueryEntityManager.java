package ru.itshool.phonebook.entities.handlers;

import org.axonframework.eventsourcing.EventSourcingHandler;
import org.axonframework.eventsourcing.EventSourcingRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import ru.itshool.phonebook.aggregates.ContactAggregate;
import ru.itshool.phonebook.aggregates.ContactType;
import ru.itshool.phonebook.entities.ContactQueryEntity;
import ru.itshool.phonebook.entities.repositories.ContactRepository;
import ru.itshool.phonebook.events.BaseEvent;

@Component
public class ContactQueryEntityManager {
    @Autowired
    private ContactRepository contactRepository;

    @Autowired
    @Qualifier("contactAggregateEventSourcingRepository")
    private EventSourcingRepository<ContactAggregate> contactAggregateEventSourcingRepository;

    @EventSourcingHandler
    void on(BaseEvent event){
        persistContact(buildQueryContact(getContactFromEvent(event)));
    }


    private ContactAggregate getContactFromEvent(BaseEvent event){
        return contactAggregateEventSourcingRepository.load(event.getId().toString()).getWrappedAggregate().getAggregateRoot();
    }

    private ContactQueryEntity findExistingOrCreateQueryContact(String id){
        return contactRepository.findById(id).isPresent() ? contactRepository.findById(id).get() : new ContactQueryEntity();
    }

    private ContactQueryEntity buildQueryContact(ContactAggregate contactAggregate){
        ContactQueryEntity contactQueryEntity = findExistingOrCreateQueryContact(contactAggregate.getId());

        contactQueryEntity.setId(contactAggregate.getId());
        contactQueryEntity.setContactType(ContactType.valueOf(contactAggregate.getContactType()));
        contactQueryEntity.setValue(contactAggregate.getValue());
        contactQueryEntity.setPersonId(contactAggregate.getPersonId());

        return contactQueryEntity;
    }

    private void persistContact(ContactQueryEntity contactQueryEntity){
        contactRepository.save(contactQueryEntity);
    }
}
