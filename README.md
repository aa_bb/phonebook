Интерфейс: http://localhost:8080/swagger-ui.html

База данных http://localhost:8080/h2-console


Первое знакомство с CQRS + EventSourcing, разбиралась по этому гайду: http://progressivecoder.com/event-sourcing-and-cqrs-with-axon-and-spring-boot-part-2/.
Использовалась связка Spring + Axon.

+ Добавить персону.

+ Удалить персону.

+ Найти персону по любому из атрибутов (атрибуты персоны: ФИО, дата рождения; полнотекстовый поиск).

+ Создать контакт у персоны необходимого типа: e-mail, телефон, адрес, skype.

+ Поиск персоны по контактным данным.

+ Поиск дублей персон/контактов. Возможность объединения контактов двух персон в одну.
 
- Выгрузка записной книжки в текстовый файл csv. (getAll)

+ Для каждого типа контактов предопределен набор действий (позвонить, отправить sms, позвонить по skype, отправить письмо по e-mail)
 